import Validator from "instant-validation";
import { requiredRule, lengthRule } from "instant-validation/rules";

export const pattern = r => v => {
  const regexp = new RegExp(r);

  return regexp.test(v);
};


export const emailRule = pattern(
  /^[A-z0-9.!#$%&‘*+/=?^_`{|}~-]+@[A-z0-9]+(?:\.[A-z0-9]+)*$/
);


export const validator = new Validator({
  name: [
    {
      rule: requiredRule,
      message: "Please enter your name"
    },
    {
      rule: lengthRule(2),
      message: "Your name should be at least 2 characters"
    }
  ],
  email: [
    {
      rule: requiredRule,
      message: "Please enter an email"
    },
    {
      rule: emailRule,
      message: "Please enter correct email"
    },
  ],
  country: [
    {
      rule: requiredRule,
      message: "Please enter country"
    }
  ],
  password: [
    {
      rule: requiredRule,
      message: "Please enter password"
    },
    {
      rule: lengthRule(8),
      message: "Your password should be at least 8 characters"
    }
  ],
  passwordConfirmation: [
    {
      rule: requiredRule,
      message: "Please repeat password"
    },
    {
      rule: (value, passwordOriginal) => value === passwordOriginal,
      message: "Passwords are not equal",
      ruleId: "passwordEqual"
    }
  ],
  phone: [
    {
      rule: requiredRule,
      message: "Please repeat password"
    },
    {
      rule: lengthRule(11),
      message: "Your phone should be at least 11 characters"
    }
  ]
});