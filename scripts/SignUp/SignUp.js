import React, { useState, useEffect } from "react";

import { Input } from "../Input/Input";
import { Checkbox } from "../Checkbox/Checkbox";
import { Button } from "../Button/Button";
import { Select } from "../Select/Select";
import { PhoneNumber } from "../PhoneNumber/PhoneNumber";
import { Notification } from "../Notification/Notification";

import * as styles from "./SignUp.scss";

import { validator } from "./validation";

const fetchCountries = async callback => {
  const response = await fetch("http://0.0.0.0:3002/countries");
  const res = await JSON.parse(await response.text());
  callback(res);
};

const sendData = async data => {
  const headers = new Headers([["Content-Type", "application/json"]]);
  const response = await fetch("http://0.0.0.0:3002/register", {
    method: "POST",
    body: JSON.stringify(data),
    headers
  });
  return await JSON.parse(await response.text());;
};

export const SignUp = () => {
  const [values, setInputValue] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    passwordConfirmation: "",
    check: false,
    country: ""
  });
  const [countries, setCountries] = useState([]);
  const [notifiication, setNotification] = useState({
    message: "",
    show: false,
    status: ""
  });

  useEffect(() => {
    fetchCountries(setCountries);
  }, []);

  const handleChange = e => {
    const { name: key, value, type, checked } = e.target;
    setInputValue({
      ...values,
      [key]: type === "checkbox" ? checked : value
    });
  };

  const { errors } = validator
    .insertArgs({
      passwordEqual: [values.passwordConfirmation]
    })
    .validate(values);

  const mappedCountries = countries.map(item => ({
    id: item.country_code,
    name: item.name
  }));

  return (
    <div className={styles.root}>
      <h1 className={styles.title}>Sign up</h1>
      <form
        className={styles.form}
        onSubmit={async (e) => {
          e.preventDefault();
          const res = await sendData(values);

          res.status === 'success' && setNotification({
            show: true,
            message: "your account has been successful created.",
            status: "Great!"
          })
        }}
      >
        <Input
          name="name"
          value={values.name}
          onChange={handleChange}
          label="Name"
          error={errors.name}
        />
        <PhoneNumber
          name="phone"
          onChange={handleChange}
          value={values.phone}
          error={errors.phone}
        />
        <Input
          name="email"
          value={values.email}
          onChange={handleChange}
          label="Email address"
          error={errors.email}
        />
        <Select
          name="country"
          label="Select country"
          options={mappedCountries}
          value={values.country}
          onChange={handleChange}
          error={errors.country}
        />
        <Input
          type="password"
          name="password"
          value={values.password}
          onChange={handleChange}
          label="Password"
          error={errors.password}
        />
        <Input
          type="password"
          name="passwordConfirmation"
          value={values.passwordConfirmation}
          onChange={handleChange}
          label="Password сonfirmation"
          error={errors.passwordConfirmation}
        />
        <Checkbox
          name="check"
          checked={values.check}
          onChange={handleChange}
          label="Yes, I'd like to recieve the very occasional email with information on new services and discounts"
        />
        <Button
          type="submit"
          text="create an account"
          disabled={!validator.isFormValid()}
        />
      </form>
      <p>
        Already have a 24Slides account? <a href="#">Click here</a> to log in to
        your existing account and join a company team
      </p>
      {notifiication.show && <Notification message={notifiication.message} status={notifiication.status} onClose={() => setNotification({ show: false })}/>}
    </div>
  );
};
