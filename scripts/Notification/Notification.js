import React, { useEffect } from 'react';

import check from '../../icons/check.svg';
import close from '../../icons/close.svg';

import * as styles from './Notification.scss';


export const Notification = ({ message, onClose, status }) => {
  useEffect(() => {
    setTimeout(() => onClose(), 3000);
  },[]);

  return <div className={styles.root}>
    <img className={styles.statusIcon} src={check} alt="statusIcon"/>
    <h3 className={styles.title}>{status}</h3>
    <span className={styles.text}>{message}</span>
    <img className={styles.closeIcon} src={close} alt="closeIcon"/>
  </div>
};