import React, { useState } from 'react';
import * as styles from './Checkbox.scss';

export const Checkbox = (props) => {
    return (
        <div className={styles.root}>
            <input
                name={props.name}
                value={props.name}
                id={props.name}
                type="checkbox"
                checked={props.checked}
                onChange={props.onChange}
            />
            <label className={styles.label} htmlFor={props.name}>{props.label}</label>
        </div>
    )
}