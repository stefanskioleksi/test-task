import React, { useState } from 'react';
import * as styles from './Button.scss';

export const Button = (props) => {
    return (
        <button
            disabled={props.disabled}
            className={`${styles.button} ${props.disabled ? styles.disabled : ""} `}
            type={props.type}
            onClick={props.onClick}
        >
            {props.text}
        </button>
    )
}

