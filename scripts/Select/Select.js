import React, { useState, useEffect } from "react";
import * as styles from "./Select.scss";

const generateEventObject = (value, name) => ({
  target: {
    value,
    name
  }
});

export const Select = ({ options, value, label, onChange, name, error, toggleHandler = () => null }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    toggleHandler(open)
  }, [open]);

  const handleChange = value => {
    onChange(generateEventObject(value, name));
    setOpen(false);
  };

  const { name: chosenOption } = options.find(item => (item.id === value)) || {};
  const showError = !open && error;
  const isCorrect = !!value && !error;

  return (
    <div
      className={styles.root}
      tabIndex={-1}
      onClick={() => setOpen(!open)}
      onBlur={() => setOpen(false)}
    >
      <div
        className={`${styles.select} ${
          open
            ? styles.selectOpen
            : showError
            ? styles.selectError
            : isCorrect
            ? styles.selectCorrect
            : ""
        }`}
      >
        {chosenOption || label}
      </div>
      {open && (
        <div className={styles.list} onClick={e => e.stopPropagation()}>
          {options.map(({ id, name }) => (
            <span
              key={id}
              className={styles.option}
              onClick={() => handleChange(id)}
            >
              {name}
            </span>
          ))}
        </div>
      )}
      {showError && <div className={styles.error}>{error}</div>}
    </div>
  );
};
