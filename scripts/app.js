import React, { Component } from 'react'
import { hot } from 'react-hot-loader'

import * as styles from './app.scss';
import { SignUp } from 'SignUp/SignUp';

class App extends Component {
  render() {
    return (
      <div className={styles.root}>
        <SignUp/>
      </div>
    )
  }
}

export default hot(module)(App)