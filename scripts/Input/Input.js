import React, { useState, useEffect } from 'react';
import * as styles from './Input.scss';

export const Input = (props) => {
    const [focus, setFocus] = useState(false);

    useEffect(() => {
        props.focusHandler && props.focusHandler(focus);
    }, [focus]);

    const showError = !focus && props.error;
    const notEmpty = !!props.value;
    const labelIsTop = focus || notEmpty;
    const isCorrect = !showError && notEmpty && !focus;

    const inputClass = `${styles.input} ${focus ? styles.inputFocus : ""} ${showError ? styles.inputError : isCorrect ? styles.inputCorrect : ""}`;

    return <div className={styles.root} >
        <label className={`${styles.label} ${labelIsTop ? styles.labelTop : ""}`} htmlFor={props.name}>{props.label}</label>
        <input className={inputClass}
            id={props.name}
            type={props.type}
            name={props.name}
            onChange={props.onChange}
            value={props.value}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
        />
        {showError && <div className={styles.error}>{props.error}</div>}
    </div>

};