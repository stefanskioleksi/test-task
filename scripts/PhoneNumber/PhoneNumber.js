import React, { useState } from 'react';
import * as styles from './PhoneNumber.scss'

import { Input } from '../Input/Input';
import { Select } from '../Select/Select';

const phoneCodes = [
  {
    id: '+380',
    name: '+380'
  },
  {
    id: '+7',
    name: '+7'
  }
];

export const PhoneNumber = ({ name, onChange, error }) => {
  const [code, setCode] = useState("");
  const [phone, setPhone] = useState("");
  const [focus, setFocus] = useState(false);

  const handleCodeChange = e => {
    const { value } = e.target;
    setCode(value);
    onChange({
      target: {
        name,
        value: value + phone
      }
    })
  };

  const handlePhoneChange = e => {
    const { value } = e.target;
    setPhone(value);
    onChange({
      target: {
        name,
        value: code + value
      }
    })
  };

  const haveBothValues = code && phone;
  const isCorrect = !error && !focus && haveBothValues;
  const showError = !focus && error;

  return <div className={`${styles.root} ${focus ? styles.rootFocus : showError ? styles.rootError : isCorrect ? styles.rootCorrect : ""}`}>
    <Select name="code" value={code} onChange={handleCodeChange} label="Code" options={phoneCodes} toggleHandler={setFocus}/>
    <Input name="phone" value={phone} onChange={handlePhoneChange} lable="Phone number" focusHandler={setFocus}/>
    { showError && <div className={styles.error}>{error}</div> }
  </div>
};